enterReplies = ['Hi', 'Target Acquired', 'Firing', 'Hello friend.', 'Gotcha', 'I see you']
leaveReplies = ['Are you still there?', 'Target lost', 'Searching']

module.exports = (robot) ->
  robot.enter (res) ->
    res.send res.random enterReplies

  robot.leave (res) ->
    res.send res.random leaveReplies

  robot.hear /badger/i, (res) ->
    # your code here

  robot.respond /open the (.*) doors/i, (res) ->
    doorType = res.match[1]

    if doorType is "pod bay"
      res.reply "I'm afraid I can't let you do that."
    else
      res.reply "Opening #{doorType} doors"
