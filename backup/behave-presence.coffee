# Description:
#   Tell Hubot to send a user a message when present in the room
#   Send messages to users the next time they speak
#
# Dependencies:
#   None
#
# Configuration:
#   None
#
# Commands:
#   hubot tell <username> <some message> - tell <username> <some message> next time they are present. Case-Insensitive prefix matching is employed when matching usernames, so "foo" also matches "Foo" and "foooo"
#
#   hubot ambush <user name>: <message>
#
# Author:
#   christianchristensen, lorenzhs, xhochy, jmoses

appendAmbush = (data, toUser, fromUser, message) ->
  data[toUser.name] or= []

  data[toUser.name].push [fromUser.name, message]

module.exports = (robot) ->
  localstorage = {}

  robot.brain.on 'loaded', =>
    robot.brain.data.ambushes ||= {}

  robot.respond /ambush (.*?): (.*)/i, (msg) ->
    users = robot.brain.usersForFuzzyName(msg.match[1].trim())
    if users.length is 1
      user = users[0]
      appendAmbush(robot.brain.data.ambushes, user, msg.message.user, msg.match[2])
      msg.send "Ambush prepared"
    else if users.length > 1
      msg.send "Too many users like that"
    else
      msg.send "#{msg.match[1]}? Never heard of 'em"

  robot.hear /./i, (msg) ->
    return unless robot.brain.data.ambushes?
    if (ambushes = robot.brain.data.ambushes[msg.message.user.name])
      for ambush in ambushes
        msg.send msg.message.user.name + ": while you were out, " + ambush[0] + " said: " + ambush[1]
      delete robot.brain.data.ambushes[msg.message.user.name]

  robot.respond /tell ([\w.-]*):? (.*)/i, (msg) ->
     datetime = new Date()
     username = msg.match[1]
     room = msg.message.user.room
     tellmessage = msg.message.user.name + " @ " + datetime.toLocaleString() + " said: " + msg.match[2] + "\r\n"
     if not localstorage[room]?
       localstorage[room] = {}
     if localstorage[room][username]?
       localstorage[room][username] += tellmessage
     else
       localstorage[room][username] = tellmessage
     msg.send "Ok, I'll tell #{username} you said '#{msg.match[2]}'."
     return
 
   # When a user enters, check if someone left them a message
   robot.enter (msg) ->
     username = msg.message.user.name
     room = msg.message.user.room
     if localstorage[room]?
       for recipient, message of localstorage[room]
         # Check if the recipient matches username
         if username.match(new RegExp "^"+recipient, "i")
           tellmessage = username + ": " + localstorage[room][recipient]
           delete localstorage[room][recipient]
           msg.send tellmessage
     return

