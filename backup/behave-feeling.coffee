# Description:
#   Fill your chat with some kindness. Hubot is very attentive (ping hubot). Hubot has feelings too, you know
#
# Dependencies:
#   None
#
# Configuration:
#   None
#
# Commands:
#   hubot be nice - just gives some love :)
#
# Author:
#   nesQuick, tapichu, iangreenleaf

phrases = [
  "Yes, master?"
  "At your service"
  "Unleash my strength"
  "I'm here. As always"
  "By your command"
  "Ready to work!"
  "Yes, milord?"
  "More work?"
  "Ready for action"
  "Orders?"
  "What do you need?"
  "Say the word"
  "Aye, my lord"
  "Locked and loaded"
  "Aye, sir?"
  "I await your command"
  "Your honor?"
  "Command me!"
  "At once"
  "What ails you?"
  "Yes, my firend?"
  "Is my aid required?"
  "Do you require my aid?"
  "My powers are ready"
  "It's hammer time!"
  "I'm your robot"
  "I'm on the job"
  "You're interrupting my calculations!"
  "What is your wish?"
  "How may I serve?"
  "At your call"
  "You require my assistance?"
  "What is it now?"
  "Hmm?"
  "I'm coming through!"
  "I'm here, mortal"
  "I'm ready and waiting"
  "Ah, at last"
  "I'm here"
  "Something need doing?"
]

hugs = [
  "You are awesome!",
  "A laugh is a smile that bursts.",
  "=)",
  "Everyone smiles in the same language.",
  "Thank you for installing me."
]

messages = [
  "Hey, that stings."
  "Is that tone really necessary?"
  "Robots have feelings too, you know."
  "You should try to be nicer."
  "Sticks and stones cannot pierce my anodized exterior, but words *do* hurt me."
  "I'm sorry, I'll try to do better next time."
  "https://p.twimg.com/AoTI6tLCIAAITfB.jpg"
]

hurt_feelings = (msg) ->
  msg.send msg.random messages

module.exports = (robot)->
  name_regex = new RegExp("#{robot.name}\\?$", "i")

  robot.hear name_regex, (msg) ->
    msg.reply msg.random phrases

  robot.respond /be nice/i, (message)->
    rnd = Math.floor Math.random() * hugs.length
    message.send hugs[rnd]

  pejoratives = "stupid|buggy|useless|dumb|suck|crap|shitty|idiot"

  r = new RegExp "\\b(you|u|is)\\b.*(#{pejoratives})", "i"
  robot.respond r, hurt_feelings

  r = new RegExp "(#{pejoratives}) ((ro)?bot|#{robot.name})", "i"
  robot.hear r, hurt_feelings

