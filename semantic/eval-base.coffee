# Description:
#   Base64 encoding and decoding
#
# Dependencies:
#   "big-integer": "1.1.5"
#
# Configuration:
#   None
#
# Commands:
#   hubot base36 e(ncode)|d(ecode) <query> - Base36 encode or decode <query>
#   hubot base58 encode|decode <query> - Base58 encode or decode <query>
#   hubot base64 encode|decode <query> - Base64 encode or decode <string>
#
# Author:
#   jimeh

module.exports = (robot) ->
  robot.hear /base36 e(ncode)?( me)? (.*)/i, (msg) ->
    try
      msg.send Base36.encode(msg.match[3])
    catch e
      throw e unless e.message == 'Value passed is not an integer.'
      msg.send "Base36 encoding only works with Integer values."

  robot.hear /base36 d(ecode)?( me)? (.*)/i, (msg) ->
    try
      msg.send (String) Base36.decode(msg.match[3])
    catch e
      throw e unless e.message == 'Value passed is not a valid Base36 string.'
      msg.send "Not a valid base36 encoded string."

  #*****************************************************************************

  robot.respond /base64 encode( me)? (.*)/i, (msg) ->
    msg.send new Buffer(msg.match[2]).toString('base64')

  robot.respond /base64 decode( me)? (.*)/i, (msg) ->
    msg.send new Buffer(msg.match[2], 'base64').toString('utf8')

  #*****************************************************************************

  robot.respond /base58 encode( me)? (.*)/i, (msg) ->
    try
      msg.send Base58.encode(msg.match[2])
    catch e
      throw e unless e.message == 'Value passed is not an integer.'
      msg.send "Base58 encoding only works with Integer values."

  robot.respond /base58 decode( me)? (.*)/i, (msg) ->
    try
      msg.send Base58.decode(msg.match[2])
    catch e
      throw e unless e.message == 'Value passed is not a valid Base58 string.'
      msg.send "Not a valid base58 encoded string."

class Base58Builder
  constructor: ->
    @alphabet = "123456789abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNPQRSTUVWXYZ"
    @base = @alphabet.length

  encode: (num) ->
    throw new Error('Value passed is not an integer.') unless /^\d+$/.test num
    num = parseInt(num) unless typeof num == 'number'
    str = ''
    while num >= @base
      mod = num % @base
      str = @alphabet[mod] + str
      num = (num - mod)/@base
    @alphabet[num] + str

  decode: (str) ->
    num = 0
    for char, index in str.split("").reverse()
      if (char_index = @alphabet.indexOf(char)) == -1
        throw new Error('Value passed is not a valid Base58 string.')
      num += char_index * Math.pow(@base, index)
    num

Base58 = new Base58Builder()
